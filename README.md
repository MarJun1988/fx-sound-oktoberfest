# FxSounds - Offizielle Website des Oktoberfests im Freizeitbad Erdmannsdorf

Diese Seite ist für die Bekanntmachung, 
des Oktoberfestes im Freizeitbad Erdmannsdorf welche von FxSound ausgerichtet wird gedacht.

### Hinweis

- [FxSound](https://Fxsound.de) (Veranstallter)
- [Bergluft](https://www.berg-luft.de) (Teilnehmende Band)
- M. Junghans (Ersteller der Website)

### Dieses Projekt wurde mit folgenden Komponenten erstellt:

- VUE JS 3
- VUE Route
- VUE Store
- BalmUi
- externe Schriften (lokal Eingebunden)

## Projekt Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Compile and Minify for Production with Host Address

```sh
npm run host
```


### Compile and Minify for Production

```sh
npm run build
```
