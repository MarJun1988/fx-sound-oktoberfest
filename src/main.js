/* VUE JS  */
import {createApp} from 'vue'
import App from './App.vue'

/* Store Pinia */
import {createPinia} from 'pinia'

/* Vue Router */
import Router from './router'

/* Vue 3 Framework BalmUI */
import BalmUI from 'balm-ui'; // Official Google Material Components
import BalmUIPlus from 'balm-ui-plus'; // BalmJS Team Material Components
import 'balm-ui-css';

/* Import eigenen Style */
import "./assets/style/main.scss"

const app = createApp(App)

app
    .use(createPinia())
    .use(Router)
    .use(BalmUI, {
        $theme: {
            'primary': '#212121',
            // 'secondary': '#ec407a',
            // 'background': '#9c27b0',
            // 'surface': '#1e88e5',
            // 'error': '#b71c1c',
            // 'on-primary': '#bdbdbd',
            // 'on-secondary': '#f48fb1',
            // 'on-surface': '#43a047',
            // 'on-error': '#e57373'
        }
    })
    .use(BalmUIPlus)
    .mount('#app')