import {createRouter, createWebHistory} from 'vue-router'

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: '/',
            name: 'home',
            component: () => import('../views/HomeView.vue'),
            meta: {
                viewName: 'Start',
                class: 'col-6 sm:col-3 md:col-3 lg:col-3 xl:col-3'
            }
        },
        {
            path: '/impressum',
            name: 'impressum',
            component: () => import('../views/ImpressumView.vue'),
            meta: {
                viewName: 'Impressum',
                class: 'col-6 sm:col-3 md:col-3 lg:col-3 xl:col-3'
            }
        },
        {
            path: '/data-protection',
            name: 'dataProtection',
            component: () => import('../views/DataProtection.vue'),
            meta: {
                viewName: 'Datenschutz',
                class: 'col-6 sm:col-3 md:col-3 lg:col-3 xl:col-3'
            }
        },
        {
            path: '/question-tickets',
            name: 'questionTickets',
            component: () => import('../views/QuestionTickets.vue'),
            meta: {
                viewName: 'Firmentickets',
                class: 'col-6 sm:col-3 md:col-3 lg:col-3 xl:col-3'
            }
        }
    ]
})

export default router
