<?php
declare(strict_types=1);
header('Content-Type: application/json; charset=utf-8');

const EMPFAENGER = "fxsound@web.de";
//const EMPFAENGER = "marcel.junghans@gmx.net";
const EMPFAENGER_NAME = "Fxsound - Anfrage";
const REPLAY_TO = "fxsound@web.de";
const SUBJECT = "Anfrage Oktoberfest Firmentickets";
const QUESTION_SITE = "https://oktoberfest-erdmannsdorf.de/question-tickets";

const ENCODING = "utf-8";

$_POST = json_decode(file_get_contents('php://input'), true);

if ($_SERVER["REQUEST_METHOD"] === "POST" &&
    isset($_POST['companyName']) &&
    isset($_POST['countOfTickets']) &&
    isset($_POST['nameOfPoc']) &&
    isset($_POST['mailOfPoc']) &&
    isset($_POST['telefonOfPoc']) &&
    isset($_POST['message'])
) {

    $companyName = htmlspecialchars($_POST['companyName']);
    $countOfTickets = htmlspecialchars($_POST['countOfTickets']);
    $nameOfPoc = htmlspecialchars($_POST['nameOfPoc']);
    $mailOfPoc = htmlspecialchars($_POST['mailOfPoc']);
    $telefonOfPoc = htmlspecialchars($_POST['telefonOfPoc']);
    $message = htmlspecialchars($_POST['message']);
    $htmlBody = '
<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Anfrage Oktoberfest Firmentickets</title>
</head>
<body>
    <h4>Anfrage Firmentickets Oktoberfest 2023</h4>
    <br>
    <p>
        Diese Anfrage wurde über <a href="' . QUESTION_SITE . '" target="_blank">' . QUESTION_SITE . '</a> gestellt.
    </p>
    <br>
    <br>
    <h4>Der Ansprechpartner ' . $nameOfPoc . ' hat folgenden Text eingegeben</h4>
    <br>
    <p>' . $message . '</p>
    <br>
    <span><b>Firmenname:</b> ' . $companyName . '</span>
    <br>
    <span><b>Ansprechpartner:</b> ' . $nameOfPoc . '</span>
    <br>
    <span><b>E-Mail Adresse:</b> ' . $mailOfPoc . '</span>
    <br>
    <span><b>Rufnummer:</b> ' . $telefonOfPoc . '</span>
</body>
</html>
';
    $header = "Content-type: text/html; charset=" . ENCODING . " \r\n";
    $header .= "MIME-Version: 1.0 \r\n";
    $header .= "Content-Transfer-Encoding: 8bit \r\n";
    $header .= "Date: " . date("r (T)") . " \r\n";

    try {
        $mail = [
            "status" => false,
            "companyName" => $companyName,
            "countOfTickets" => $countOfTickets,
            "nameOfPoc" => $nameOfPoc,
            "mailOfPoc" => $mailOfPoc,
            "telefonOfPoc" => $telefonOfPoc,
            "message" => $message,
            "sendMessage" => "Es ist leider ein Fehler aufgetreten."
        ];

        if (mail(EMPFAENGER, SUBJECT, $htmlBody, $header, "-f " . $mailOfPoc)) {
            $mail['status'] = true;
            $mail['sendMessage'] = "Ihre Anfrage würde erfolgreich übermittelt!";
            echo json_encode($mail, JSON_THROW_ON_ERROR);
        } else {
            echo json_encode($mail, JSON_THROW_ON_ERROR);
        }
    } catch (Exception $exception) {
//        echo json_encode($exception->getMessage());
    }
} else {
    echo json_encode("Es würden keine Eingaben übergeben.");
}