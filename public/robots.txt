# Example 1: Block only Googlebot
User-agent: Googlebot
Disallow: /
Sitemap: https://oktoberfest-erdmannsdorf.de/sitemap.xml

# Example 2: Block Googlebot and Adsbot
User-agent: Googlebot
User-agent: AdsBot-Google
Disallow: /
Sitemap: https://oktoberfest-erdmannsdorf.de/sitemap.xml

# Example 3: Block all crawlers except AdsBot (AdsBot crawlers must be named explicitly)
User-agent: *
Disallow: /
Sitemap: https://oktoberfest-erdmannsdorf.de/sitemap.xml